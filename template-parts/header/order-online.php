<?php
    $menu = get_page_by_path('menu');
    $link = get_field('menu_link', $menu);
    if( $link ): 
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

    <div class="cta">
        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
    </div>

<?php endif; ?>
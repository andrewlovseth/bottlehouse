<?php
    $menu = get_page_by_path('menu');
    $reservations = get_field('reservations', $menu);
    $active = $reservations['active'];
    $label = $reservations['label'];
    $embed = $reservations['embed'];

    if($active):
?>

<div class="cta resy-link">
    <a href="https://resy.com/cities/sea/bottlehouse" id="resyButton-julh8CzyvhTBKO-36CCZh"><?php echo $label; ?></a>
</div>

<?php endif; ?>


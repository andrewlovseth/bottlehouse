<?php

    $home = get_option('page_on_front');

    if(get_field('banner', $home) == true):

?>

    <section id="banner">
        <div class="wrapper">

            <?php if(have_rows('banners', $home)): while(have_rows('banners', $home)): the_row(); ?>

                <?php 

                    $link = get_sub_field('link');
                    $mobile = get_sub_field('mobile');
                    $desktop = get_sub_field('desktop');

                ?>
            
                <div class="banner-entry">

                    <?php if($link): ?>
                        <a href="<?php echo $link; ?>">
                    <?php endif; ?>

                        <span class="mobile">
                            <?php echo wp_get_attachment_image($mobile['ID'], 'full'); ?>
                        </span>

                        <span class="desktop">
                            <?php echo wp_get_attachment_image($desktop['ID'], 'full'); ?>
                        </span>

                    <?php if($link): ?>
                        </a>
                    <?php endif; ?>

                </div>

            <?php endwhile; endif; ?>

        </div>
    </section>

<?php endif; ?>
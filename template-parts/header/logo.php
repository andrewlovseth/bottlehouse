<section class="logo">
    <div class="mobile">
        <a href="<?php echo site_url('/'); ?>">
            <img src="<?php $image = get_field('mobile_header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>
    </div>

    <div class="desktop">
        <a href="<?php echo site_url('/'); ?>">
            <img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </a>
    </div>
</section>
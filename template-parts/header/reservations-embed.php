<?php
    $menu = get_page_by_path('menu');
    $reservations = get_field('reservations', $menu);
    $active = $reservations['active'];
    $embed = $reservations['embed'];


    if($active):
?>

    <?php echo $embed; ?>

<?php endif; ?>

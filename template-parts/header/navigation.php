<nav>
    <a href="<?php echo site_url('/'); ?>" class="nav-home">Home</a>
    
    <?php if(have_rows('navigation', 'options')): while(have_rows('navigation',  'options')): the_row(); ?>

        <?php 
            $link = get_sub_field('link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <a class="nav-<?php echo sanitize_title_with_dashes($link_title); ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

        <?php endif; ?>

    <?php endwhile; endif; ?>

    <div class="social mobile">
        <?php get_template_part('partials/social-links'); ?>
    </div>
</nav>
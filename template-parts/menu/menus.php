<?php if(have_rows('menus')): while(have_rows('menus')) : the_row(); ?>

	<?php if( get_row_layout() == 'menu' ): ?>

		<section id="<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>" class="menu">
			<div class="wrapper">

				<div class="mobile">
					<img src="<?php $image = get_sub_field('mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
				<div class="desktop">
					<img src="<?php $image = get_sub_field('desktop'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			</div>
		</section>

	<?php endif; ?>

<?php endwhile; endif; ?>
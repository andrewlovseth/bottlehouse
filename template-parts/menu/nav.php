<section class="menu-nav">
	<div class="wrapper">

		<nav>
			<?php if(have_rows('menus')): while(have_rows('menus')) : the_row(); ?>

				<?php if( get_row_layout() == 'menu' ): ?>

					<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>"><?php echo get_sub_field('name'); ?></a>

				<?php endif; ?>

			<?php endwhile; endif; ?>
		</nav>		

	</div>
</section>
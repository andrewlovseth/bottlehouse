<section class="menu-cta">
	<div class="wrapper">

		<div class="menu-cta__grid">
			<?php 
				$link = get_field('menu_link');
				if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>

				<div class="cta">
					<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				</div>

			<?php endif; ?>

			<?php get_template_part('template-parts/header/reservations'); ?>
		</div>

	</div>			
</section>
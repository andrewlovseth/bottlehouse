<?php

/*

	Template Name: Menu

*/

get_header(); ?>
	
	<?php get_template_part('partials/page-header'); ?>

	<?php get_template_part('template-parts/menu/cta'); ?>

	<?php get_template_part('template-parts/menu/nav'); ?>

	<?php get_template_part('template-parts/menu/menus'); ?>

<?php get_footer(); ?>
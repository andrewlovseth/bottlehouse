<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero">
		<div class="wrapper">

			<div class="info">
				<div class="hours">
					<h4>Hours</h4>
					<div class="daily-hours">
						<p class="mobile"><?php echo get_field('daily_hours', 'options'); ?></p>
					</div>

					<?php if(get_field('happy_hour', 'options')): ?>
						<div class="happy-hour">
							<p>
								Happy Hour<br/>
								<?php echo get_field('happy_hour', 'options'); ?>
							</p>
						</div>
					<?php endif; ?>
				</div>

				<div class="contact-info">
					<div class="neighborhood">
						<h4><?php echo get_field('neighborhood', 'options'); ?></h4>
					</div>

					<div class="address">
						<p><?php echo get_field('address', 'options'); ?></p>
					</div>

					<div class="phone">
						<p><?php echo get_field('phone', 'options'); ?></p>
					</div>
				</div>
			</div>

			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			
		</div>
	</section>

	<section id="features">
		<div class="wrapper">
			
			<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
			 
			    <div class="feature">
			    	<div class="photo">
				        <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    </div>

				    <div class="info">
						<?php $info = get_sub_field('info'); ?>
						<p><?php echo $info['blurb']; ?></p>
				    </div>
			    </div>

			<?php endwhile; endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/mailchimp-form'); ?>


<?php get_footer(); ?>
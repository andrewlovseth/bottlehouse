<?php

/*

	Template Name: Wine Club

*/

get_header(); ?>


	<?php get_template_part('partials/page-header'); ?>


	<?php get_template_part('partials/gallery-header'); ?>



	<section id="content">
		<div class="wrapper">

			<div class="thirsty-club-headline">
				<img src="<?php $image = get_field('thirsty_club_headline'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				
			</div>
			
			<div class="thirsty-club-description">
				<?php echo get_field('thirsty_club_description'); ?>

				<?php $link = get_field('membership_form');  if( $link ): ?>
					<div class="cta">
						<?php
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						?>

						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>

					</div>
				<?php endif; ?>
			</div>

			<div class="perks">
				<div class="desktop">
					<img src="<?php $image = get_field('perks_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="mobile">
					<img src="<?php $image = get_field('perks_graphic_mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
			</div>
			
		</div>
	</section>


<?php get_footer(); ?>
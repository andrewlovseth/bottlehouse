(function ($, window, document, undefined) {
    $(document).ready(function ($) {
        // rel="external"
        $('a[rel="external"]').click(function () {
            window.open($(this).attr("href"));
            return false;
        });

        // Menu Toggle
        $("#toggle").click(function () {
            $("header, header nav").toggleClass("open");
            return false;
        });

        $(".menu-nav a").smoothScroll();

        $("#newsletter a").on("click", function () {
            $(".modal").removeClass("modal__off");

            return false;
        });

        $(".modal__close").on("click", function () {
            $(".modal").addClass("modal__off");

            return false;
        });
    });

    $(document).mouseup(function (e) {
        var modal = $(".modal__wrapper");

        if (!modal.is(e.target) && modal.has(e.target).length === 0) {
            $(".modal").addClass("modal__off");
        }
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $(".modal").addClass("modal__off");
        }
    });
})(jQuery, window, document);

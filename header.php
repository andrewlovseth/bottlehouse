<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory') ?>/images/favicon.png">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">
<?php wp_body_open(); ?>

	<header class="site-header">
		<div class="wrapper">
			<div class="order-online desktop">
				<?php get_template_part('template-parts/header/order-online'); ?>

				<?php get_template_part('template-parts/header/reservations'); ?>

			</div>
			
			<?php get_template_part('template-parts/header/logo'); ?>

			<?php get_template_part('template-parts/header/hamburger'); ?>

			<?php get_template_part('template-parts/header/navigation'); ?>

			<div class="social desktop">
				<?php get_template_part('partials/social-links'); ?>
			</div>

		</div>
	</header>

	<?php get_template_part('template-parts/header/banner'); ?>
<?php

/*

	Template Name: Host an Event

*/

get_header(); ?>


	<?php get_template_part('partials/page-header'); ?>


	<?php get_template_part('partials/gallery-header'); ?>


	<section id="content">
		<div class="wrapper">

			<div class="headline">
				<img src="<?php $image = get_field('headline'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				
			</div>
			
			<div class="description">
				<?php echo get_field('description'); ?>
			</div>

			<?php if(get_field('note')): ?>
				<div class="note">
					<?php echo get_field('note'); ?>
				</div>
			<?php endif; ?>
			
			<div class="cta">
				<div class="row row-1">
					<a href="<?php echo get_field('book_event_link'); ?>" rel="external"><?php echo get_field('book_event_label'); ?></a>
				</div>
				<div class="row row-2">
					<a href="<?php echo get_field('catering_menu_pdf'); ?>" rel="external"><?php echo get_field('catering_menu_label'); ?></a>

					<a href="<?php echo get_field('events_pdf'); ?>" rel="external"><?php echo get_field('events_label'); ?></a>
				</div>
			</div>

		</div>
	</section>


<?php get_footer(); ?>
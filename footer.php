	<footer>
		<div class="wrapper">

			<div class="divider">
				<div class="bottle">
					<img src="<?php $image = get_field('bottle', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="rule" style="background-image: url(<?php $image = get_field('rule', 'options'); echo $image['url']; ?>);">
				</div>

				<div class="emblem">
					<img src="<?php $image = get_field('emblem', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="contact-info">
				<div class="neighborhood">
					<p><?php echo get_field('neighborhood', 'options'); ?></p>
				</div>
				
				<div class="address">
					<p><?php echo get_field('address', 'options'); ?></p>
				</div>

				<div class="phone">
					<p><?php echo get_field('phone', 'options'); ?></p>
				</div>

				<div class="email">
					<p><a href="mailto:<?php echo get_field('email', 'options'); ?>"><?php echo get_field('email', 'options'); ?></a></p>
				</div>
			</div>

			<?php if(is_page_template('covid-19.php')): ?>
				<div class="main-site-link cta">
					<a href="<?php echo site_url('/home/'); ?>" rel="external">visit our old website</a>
				</div>
			<?php endif; ?>

		</div>
	</footer>
	
	<?php wp_footer(); ?>

	<?php get_template_part('template-parts/header/reservations-embed'); ?>

	<script async type="text/javascript" src="https://static.klaviyo.com/onsite/js/klaviyo.js?company_id=WgZ7mY"></script>

</body>
</html>
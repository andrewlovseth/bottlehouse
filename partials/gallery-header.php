<section id="gallery-header">
	<div class="wrapper">

		<?php $images = get_field('gallery'); if( $images ): ?>

			<div class="gallery<?php if( get_field('gallery_align') == true ): ?> right<?php endif; ?>">

				<?php $index = 1; foreach( $images as $image ): ?>

					<div class="cover image image-<?php echo $index; ?>" style="background-image: url(<?php echo $image['url']; ?>);">
					</div>

				<?php $index++; endforeach; ?>

			</div>

		<?php endif; ?>

	</div>
</section>
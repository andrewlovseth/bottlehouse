<section id="page-header">
	<div class="wrapper">
		
		<div class="image-wrapper">
			<img src="<?php $image = get_field('page_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
		
	</div>
</section>
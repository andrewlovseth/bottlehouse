<a href="<?php echo get_field('instagram', 'options'); ?>" class="instagram" rel="external">
	<img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram" />
</a>

<a href="<?php echo get_field('facebook', 'options'); ?>" class="facebook" rel="external">
	<img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebok" />
</a>

<a href="mailto:<?php echo get_field('email', 'options'); ?>" class="email">
	<img src="<?php bloginfo('template_directory') ?>/images/email.svg" alt="Email" />
</a>
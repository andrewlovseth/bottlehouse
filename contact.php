<?php

/*

	Template Name: Contact

*/

get_header(); ?>


	<?php get_template_part('partials/page-header'); ?>


	<?php get_template_part('partials/gallery-header'); ?>


	<section id="contact-info">
		<div class="wrapper">

			<div class="hours">
				<h4>Hours</h4>
				<p><?php echo get_field('hours_verbose', 'options'); ?></p>
			</div>

			<div class="address">
				<h4>Drop In</h4>
				<p><?php echo get_field('neighborhood', 'options'); ?></p>
				<p><?php echo get_field('address', 'options'); ?></p>
				<h5><a href="<?php echo get_field('directions_link', 'options'); ?>" rel="external">Get Directions</a></h5>
			</div>			

			<div class="phone">
				<h4>Ring</h4>
				<p><?php echo get_field('phone', 'options'); ?></p>
			</div>

			<div class="email">
				<h4>Email</h4>

				<h5>General Inquiries</h5>
				<p><a href="mailto:<?php echo get_field('email', 'options'); ?>"><?php echo get_field('email', 'options'); ?></a></p>

				<h5>To Book An Event</h5>
				<p><a href="mailto:<?php echo get_field('events_email', 'options'); ?>"><?php echo get_field('events_email', 'options'); ?></a></p>

				<h5>Join Our Team!</h5>
				<p><a href="mailto:<?php echo get_field('careers_email', 'options'); ?>"><?php echo get_field('careers_email', 'options'); ?></a></p>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/mailchimp-form'); ?>

<?php get_footer(); ?>
<?php

/*

	Template Name: About

*/

get_header(); ?>

	
	<?php get_template_part('partials/page-header'); ?>


	<?php get_template_part('partials/gallery-header'); ?>


	<section id="content">
		<div class="wrapper">
			
			<div class="content-wrapper">
				<?php echo get_field('content'); ?>
			</div>

			<section id="press">

				<div class="press-header image-wrapper">
					<img src="<?php $image = get_field('press_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="press-clippings-wrapper">

					<?php if(have_rows('press')): while(have_rows('press')): the_row(); ?>

					    <div class="clipping">
					        <h4><a href="<?php echo get_sub_field('link'); ?>" rel="external"><?php echo get_sub_field('publication'); ?></a></h4>
					        <p><a href="<?php echo get_sub_field('link'); ?>" rel="external"><?php echo get_sub_field('article'); ?></a></p>
					    </div>

					<?php endwhile; endif; ?>

				</div>


			</section>
	
		</div>
	</section>


<?php get_footer(); ?>
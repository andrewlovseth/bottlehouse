<?php

/*

	Template Name: Happenings

*/

get_header(); ?>

	
	<?php get_template_part('partials/page-header'); ?>

	<section id="events">
		<div class="wrapper">
			
			<?php if(have_rows('events')): while(have_rows('events')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'event' ): $info = get_sub_field('info'); ?>

					<div class="event">
						<?php if(get_sub_field('photo')): ?>
							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						<?php endif; ?>
						
						<?php if(have_rows('info')): while(have_rows('info')): the_row(); ?>			    		
							<div class="info">
								<div class="header">
									<h3><?php echo $info['day_and_time']; ?></h3>
									<h2><?php echo $info['title']; ?></h2>
									<?php if($info['subtitle']): ?>
										<h4><?php echo $info['subtitle']; ?></h4>
									<?php endif; ?>
								</div>

								<?php if($info['description']): ?>
									<div class="body">
										<p><?php echo $info['description']; ?></p>
									</div>
								<?php endif; ?>

								<div class="footer cta">
									<?php if($info['tagline']): ?>
										<h4><?php echo $info['tagline']; ?></h4>
									<?php endif; ?>
									
									<div class="links">
										<?php
											if(have_rows('links')): while(have_rows('links')): the_row(); ?>
	
											<?php 
												$link = get_sub_field('link');
												if( $link ): 
												$link_url = $link['url'];
												$link_title = $link['title'];
												$link_target = $link['target'] ? $link['target'] : '_self';
											?>

												<div class="link">
													<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
												</div>

											<?php endif; ?>

										<?php endwhile; endif; ?>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>



<?php get_footer(); ?>
<?php

/*

	Template Name: COVID-19

*/

get_header(); ?>

	<?php $bg_image = get_field('header_flower'); ?>

	<style>
		@media screen and (min-width: 768px) {
			body.page-template-covid-19 {
				background-image: url(<?php echo $bg_image['url']; ?>);
			}			
		}
	</style>

	<section class="message">
		<div class="wrapper">

			<div class="outer-border">
				<div class="inner-border">

					<div class="copy">
						<?php echo get_field('message'); ?>
					</div>

					<div class="signature">
						<img src="<?php $image = get_field('cheers'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<span><?php echo get_field('cheers_names'); ?></span>
					</div>
					
				</div>
			</div>

		</div>
	</section>

	<section class="updated-hours">
		<div class="wrapper">

			<div class="headline">
				<h3><?php echo get_field('updated_hours_headline'); ?></h3>
			</div>

			<div class="hours">
				<?php if(have_rows('updated_hours')): while(have_rows('updated_hours')): the_row(); ?>
 
				    <div class="entry">
				    	<p><?php echo get_sub_field('hours'); ?></p>
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

	<section class="details">
		<div class="wrapper">
			
			<?php if(have_rows('details')): while(have_rows('details')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'detail' ): $info = get_sub_field('info'); ?>

					<div class="detail">
						<?php if(get_sub_field('photo')): ?>
							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						<?php endif; ?>
			    		
			    		<div class="info">
			    			<?php if($info['title']): ?>
				    			<div class="header">
					    			<h3><?php echo $info['title']; ?></h3>
					    		</div>
					    	<?php endif; ?>

			    			<?php if($info['description']): ?>
				    			<div class="body">
									<?php echo $info['description']; ?>
				    			</div>
			    			<?php endif; ?>

							<?php $links = $info['links']; if($links): ?>

								<div class="footer cta">	

									<?php foreach($links as $link): ?>

										<div class="link">
											<a class="button" href="<?php echo esc_url( $link['link']['url'] ); ?>" target="<?php echo esc_attr( $link['link']['target'] ); ?>"><?php echo esc_html( $link['link']['title'] ); ?></a>
										</div>										

									<?php endforeach; ?>

								</div>

							<?php endif; ?>

			    		</div>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<section class="safety">
		<div class="wrapper">

			<div class="headline">
				<h3><?php echo get_field('safety_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php echo get_field('safety_copy'); ?>
			</div>		

		</div>
	</section>

	<div class="flower">
		<div class="photo">
			<img src="<?php $image = get_field('footer_flower'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>		
	</div>

<?php get_footer(); ?>